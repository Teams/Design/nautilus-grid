# Nautilus File Type Styling Prototype

Static page generator used to get easy file includes :/ I wish html had them already! It's 2022. However, we need [Jekyll](http://jekyllrb.com/)

The site gets built on every push, but if you want to test locally, here's how you get it set up on Fedora (or fedora toolbx):

```
#install ruby:
sudo dnf install ruby rubygem-bundler
#install all necessary gems inside the project directory
bundle install
#run jekyll server inside the project directory
bundle exec jekyll serve
```

View the live prototype [here]( https://teams.pages.gitlab.gnome.org/Design/nautilus-grid/)
