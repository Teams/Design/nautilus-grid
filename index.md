---
title: Nautilus Grid Styling Prototype
layout: default
---

# [{{ page.title }}](https://gitlab.gnome.org/Teams/Design/app-mockups/-/issues/54)

## No Thumbnails View (remote)

<ul>
{% for file in site.data.files %}
<li><div class="{{file.class}} icon">{% include_relative assets/img/{{file.class}}.svg %}</div><span>{{file.name}}</span></li>
{% endfor %}
</ul>

## Thumbnailed View

<ul class="thumbnails">
{% for file in site.data.files %}
<li>
{% if file.thumb contains "folder" %}
<div class="{{file.class}} thumbnailed" style="background-image: url('assets/thumb/{{file.thumb}}'), url('assets/img/folder-bg.svg'), linear-gradient(45deg, var(--blue3), var(--blue2));')">
{% elsif file.thumb %}
<div class="{{file.class}} thumbnailed {{file.doc}} {{file.small}}" style="background-image: url('assets/thumb/{{file.thumb}}');')">
{% else %}
<div class="{{file.class}} icon">{% include_relative assets/img/{{file.class}}.svg %} 
{% endif %}
</div><span>{{file.name}}</span></li>
{% endfor %}
</ul>
